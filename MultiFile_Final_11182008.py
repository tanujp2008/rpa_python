
import os
import math
from PyPDF2 import PdfFileReader as pfr
from wand.image import Image
from wand.color import Color
import json
import shutil

import cv2
import pytesseract
import pandas as pd
from PIL import Image as Image2
import nltk
from nltk import sent_tokenize


global js, ocr_txt
global new_text
js = open("/home/mprasha/edata_src/edb-ocr-process/OCRProcess.txt").read()
##global ocr_txt
ocr_txt = json.loads(js)
global outputFinal
outputFinal = {}
pointer=0

import stomp
import time
import sys
import os

text = None

##**************************** Sender START ****************************


def Sender_to_Queue(outputFinal):
    import time
    import sys
    import os
    import stomp


    user = os.getenv("ACTIVEMQ_USER") or "admin"
    password = os.getenv("ACTIVEMQ_PASSWORD") or "admin"
    host = os.getenv("ACTIVEMQ_HOST") or "localhost"
    port = os.getenv("ACTIVEMQ_Port") or 61616
    destination = sys.argv[1:2] or ["EDB.OUTBOUNDOCRPROCESS"]
    destination = destination[0]
    messages = 1
    conn = stomp.Connection(host_and_ports = [(host,port)])
    conn.start()
    conn.connect(login=user, passcode = password)
    print("*************Sending OutputFinal ***********************")
    final_data = []
    for i in outputFinal:
        final_data.append(outputFinal[i])
    print(final_data)
    for i in range(0,messages):
        conn.send(body = json.dumps(final_data),destination = destination, persistent = 'false')
    conn.disconnect()


##******************************* Sender End *****************************************


##******************************* PDF to IMAGE *****************************************

def create_img(self):
    ipfile = fileLocation
    print("Execution Started")
    print("***************************************************************** b")
    print("pdf to image conversion in progress...")
    print(" ")    
#Read file and count pages
    try:
        global reader
        reader = pfr(open(ipfile,'rb')) 
    except FileNotFoundError:
        print('FileNotFoundError: Please check might be file or folder location is incorrect!')
        sys.exit()
##reader = pfr(open(ipfile,'rb')) 
    page_count = reader.getNumPages()
##opfile = ipfile[:-4] + '.jpg'
    global img_folder
    img_folder = '/home/mprasha/edata_src/edb-ocr-process/Test/'
##file_path = img_folder + opfile
    images = []

    with Image(filename= ipfile, resolution=300) as img:
            img.compression_quality = 100
            img.background_color = Color('white')
            img.alpha_channel='remove'
            img.save(filename=img_folder+'img.jpg')
        
    print("*****************************************************************")
    print("PDF converted to image:")
    print(" ")
    print("*****************************************************************")
    print("Please wait while converting selection to text:")
    print(" ")
    print("*****************************************************************")
    pytesseract.pytesseract.tesseract_cmd = 'C:/Program Files (x86)/Tesseract-OCR/tesseract'
##Selection from image with given coordinates
    images = []
    img_folder = img_folder
    for img_file in os.listdir(img_folder):
            img_file = os.path.join(img_folder,img_file)
            images.append(img_file)
    print("*****************************************************************")

##***************************************************************************
##***************************************************************************




##***************************************************************************
## Define function for pdf that has only 1 page in it
##***************************************************************************

def check_for_one_page(img_folder,page_no):

    js = open("/home/mprasha/edata_src/edb-ocr-process/OCRProcess.txt").read()

    ocr_txt = {}
    ocr_txt = json.loads(js)

    cord = ["fieldZoneMinX","fieldZoneMinY","fieldZoneMaxX","fieldZoneMaxY","textExtracted"]
    keyword_received = list(final.keys())

    final_keyword = []
    final_coordinates = []
    fName=[]
    foo=1

    a = 0
    
    for i in keyword_received:
        a += 1
        
        provided_keyword = list(final.keys())[a-1]
        pn = page_no[provided_keyword]

        if pn <2:
            img_location_with_page = img_folder+'img'+'.jpg'
            image = cv2.imread(img_location_with_page)
            gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        else:
            img_location_with_page = img_folder+'img-'+str(pn)+'.jpg'
            image = cv2.imread(img_location_with_page)
            gray_image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

        lowercase_keyword = provided_keyword.lower()
        lowercase_keyword = lowercase_keyword.replace('\n',' ')
        lowercase_keyword = lowercase_keyword.replace('~', ' ')
        lowercase_keyword = lowercase_keyword.replace(':',' ')
##        final.keyword.append(i)
        count_of_keyword = len(lowercase_keyword.split())
        final_coordiantes= final.get(i)
        x1 = int(final_coordiantes[0])
        y1 = int(final_coordiantes[1])
        x2 = int(final_coordiantes[2])
        y2 = int(final_coordiantes[3])
        print(lowercase_keyword,x1,y1,x2,y2)
        crop_img = gray_image[x1:y1,x2:y2]
##        cv2.imshow("cropped", crop_img)
        text = pytesseract.image_to_string(crop_img)
        text = text.lower()
        text = text.replace('\n',' ')
        varTemp=""
        
        fieldName=""
        attachment=""
        goo=""
#Check if character is available into the selected co-ordinates. If "yes" print else check in image
        sent = nltk.sent_tokenize(text)
        for m in txt["allTemplateFields"]:
            if int(m) == allTemplateFieldIndicator: ##k:
                temp = txt["allTemplateFields"][m]
                for z in temp:
                    if z["fieldName"] == provided_keyword:
##                        print("Foo")
                        pointer=allTemplateFieldIndicator
##                        print(ocr_txt)
                        ocr_txt["extractOcrDataJson"].append({provided_keyword:text.replace(lowercase_keyword,'')})
                        ocr_txt["inputTemplateTemplateName"] = z["inputTemplateTemplateName"]                                  
                        ocr_txt["inputTemplateId"] = z["inputTemplateId"]
                        ocr_txt["clientEmailAddress"] = clientEmailAddress
                        ocr_txt["emailMessageId"] = messageId
                        ocr_txt["searchId"].append(z["fieldName"])
##                        ocr_txt["searchId"] = z["fieldName"]
                        ocr_txt["attachmentfileName"] = fileName

                                        
            else:
                if int(m) == allTemplateFieldIndicator: ##k:
                    ocr_txt["extractOcrDataJson"].append({provided_keyword:'fieldName not found'})
##                    ocr_txt["extractOcrDataJson"]={provided_keyword:'fieldName not found'}
##                    fName.append({provided_keyword:text.replace(lowercase_keyword,'')})

        varTemp = allTemplateFieldIndicator ##str(k) ##+ str(foo)
        foo+=1
        finalDict(ocr_txt,varTemp)

def finalDict(text,key):
    outputFinal.update({key:text})

    ocr_text={}
    
    
## *******************END Json file output*********************



##***************************************************************************
##                      Move files to a different folder
##***************************************************************************

def move_files(path,movete):
    files = os.listdir(path)
    files.sort()
    for f in files:
        src = path + f
        dst = moveto + f
        shutil.move(src,dst)

##***************************************************************************
##***************************************************************************


##************** Extract Values from Listener *****************************

def doOCR():

    coordinates = ["fieldZoneMinX","fieldZoneMinY","fieldZoneMaxX","fieldZoneMaxY"]

    try:
        global txt
        txt = json.loads(text)
    except TypeError:
        print('Found TypeError: Please check might be Queue is empty!')
        sys.exit()

##********** FileLocation and if template is matching *******************

    check = txt["emailAttachmentDataList"]

    j = 0
    for i in check:
        j += 1
        c = check[j-1]
        attachmentData = c["attachmentData"]
        varCheck = c["isTemplateMatch"]
        for data in attachmentData:
            if data == "fileName":
                fileName = attachmentData["fileName"]
            elif data == "fileLocation":
                fileLocation = attachmentData["fileLocation"]
            elif data == "fileExtension":
                fileExtension = attachmentData["fileExtension"]
            elif data == "id":
                allTemplateFieldIndicator = attachmentData["id"]
            elif data == "messageId":
                messageId = attachmentData["messageId"]
            elif data == "clientEmailAddress":
                clientEmailAddress = attachmentData["clientEmailAddress"]

        if fileName[-4:].lower() == '.pdf':
            fileLocation = attachmentData["fileLocation"] + attachmentData["fileName"]
        else:
            print("PDFError: Might be received file is other than pdf file")
            sys.exit()
        
##    print(fileLocation)
        txt2 = txt["allTemplateFields"]
        for k in txt2:
            if int(k) == j:
##              print('roman')
                txt3 = txt2[k]
                global final, page_no
                final = {}
                page_no = {}
                
                if varCheck == True:
                    for n in txt3:
                        final[n["fieldName"]] = [n[c] for c in coordinates]
                        page_no[n["fieldName"]] = n["pageNumebr"]

                create_img(fileLocation)
##                ocr_txt.clear()
                check_for_one_page(img_folder,page_no)
                path = "/home/mprasha/edata_src/edb-ocr-process/Test/"
                moveto = "/home/mprasha/edata_src/edb-ocr-process/Archieve/"
                move_files(path,moveto)
##Sender_to_Queue(ocr_txt)
    print("********************Sending result to ActiveMQ to send out***************************")
##print(outputFinal)
    Sender_to_Queue(outputFinal)
            
